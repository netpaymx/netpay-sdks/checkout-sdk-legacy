package mx.com.netpay.netpaysdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import static mx.com.netpay.netpaysdk.Constants.RESPONSE_ERROR;

public class CardAlertActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.netpay_card_alert);
        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        findViewById(R.id.img_netpay_sdk_ic_action_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putString(RESPONSE_ERROR,"Cancelado por el usuario");
        Intent i = new Intent();
        i.putExtras(b);
        setResult(Activity.RESULT_CANCELED, i);
        super.onBackPressed();
    }
}

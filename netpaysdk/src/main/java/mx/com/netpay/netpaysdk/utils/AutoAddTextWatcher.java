package mx.com.netpay.netpaysdk.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

public class AutoAddTextWatcher implements TextWatcher {
    private CharSequence mBeforeTextChanged;
    private TextWatcher mTextWatcher;
    private int[] mArray_pos;
    private EditText mEditText;
    private String mAppentText;

    public AutoAddTextWatcher(EditText editText, String appendText, TextWatcher textWatcher) {
        this.mEditText = editText;
        this.mAppentText = appendText;
        this.mTextWatcher = textWatcher;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        mBeforeTextChanged = s.toString();

        if (mTextWatcher != null)
            mTextWatcher.beforeTextChanged(s, start, count, after);

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(mEditText.getText().toString().startsWith("3")) {
            mArray_pos = new int[] {4,10};
        } else {
            mArray_pos = new int[] {4,8,12};
        }
        Log.d("mEditText","is amex:" + mEditText.getText().toString().startsWith("3"));

        for (int i = 0; i < mArray_pos.length; i++) {
            Log.d("mEditText","infor");
            if (((mBeforeTextChanged.length() - mAppentText.length() * i) == (mArray_pos[i] - 1) &&
                    (s.length() - mAppentText.length() * i) == mArray_pos[i])) {

                Log.d("mEditText","append45");

                mEditText.append(mAppentText);

                break;
            }

            if (((mBeforeTextChanged.length() - mAppentText.length() * i) == mArray_pos[i] &&
                    (s.length() - mAppentText.length() * i) == (mArray_pos[i] + 1))) {

                int idx_start = mArray_pos[i] + mAppentText.length() * i;
                int idx_end = Math.min(idx_start + mAppentText.length(), s.length());

                String sub = mEditText.getText().toString().substring(idx_start, idx_end);

                if (!sub.equals(mAppentText)) {
                    mEditText.getText().insert(s.length() - 1, mAppentText);
                }

                break;
            }

            if (mAppentText.length() > 1 &&
                    (mBeforeTextChanged.length() - mAppentText.length() * i) == (mArray_pos[i] + mAppentText.length()) &&
                    (s.length() - mAppentText.length() * i) == (mArray_pos[i] + mAppentText.length() - 1)) {
                int idx_start = mArray_pos[i] + mAppentText.length() * i;
                int idx_end = Math.min(idx_start + mAppentText.length(), s.length());
                mEditText.getText().delete(idx_start, idx_end);
                break;
            }
        }

        if (mTextWatcher != null)
            mTextWatcher.onTextChanged(s, start, before, count);

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mTextWatcher != null)
            mTextWatcher.afterTextChanged(s);

    }
}

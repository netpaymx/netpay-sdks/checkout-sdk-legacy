package mx.com.netpay.netpaysdk;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.threatmetrix.TrustDefender.TMXConfig;
import com.threatmetrix.TrustDefender.TMXEndNotifier;
import com.threatmetrix.TrustDefender.TMXProfiling;
import com.threatmetrix.TrustDefender.TMXProfilingConnections.TMXProfilingConnections;
import com.threatmetrix.TrustDefender.TMXProfilingConnectionsInterface;
import com.threatmetrix.TrustDefender.TMXProfilingHandle;
import com.threatmetrix.TrustDefender.TMXProfilingOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;

public abstract class BaseActivity extends AppCompatActivity {

    private final static String TAG = "BaseActivity";

    public String sessionId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            /*
             * Optionally you can configure TMXProfilingConnections, if so please pass the configured * instance to TMXConfig. On the other hand, if you prefer to use TMXProfilingConnection * default settings, there is no need to create an instance of it.
             * */

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Por favor espere");
            progressDialog.show();


            TMXProfilingConnectionsInterface profilingConnections = new TMXProfilingConnections().setConnectionTimeout(20, TimeUnit.SECONDS) // Default value is 10 seconds
                    .setRetryTimes(3); // Default value is 0 (no retry)
            /*
             * Creating a config instance to be passed to the init() method. Please note this instance
             * must include orgId and application context otherwise the init() method will fail. * */

            final boolean testMode = getIntent().getBooleanExtra(Constants.PARAMENTER_IS_TEST, true);

            TMXConfig config = new TMXConfig()
                    // For more information about configure method please see kb.threatmetrix.com // (REQUIRED) Organisation ID
                    .setOrgId(testMode?"45ssiuz3":"9ozphlqx")
                    // (REQUIRED) Enhanced fingerprint server
                    .setFPServer("h.online-metrix.net")
                    // (REQUIRED) Application Context
                    .setContext(getApplicationContext())
                    // (OPTIONAL) Pass the configured instance of TMXProfilingConnections to TMX SDK.
                    // If not passed, init() method tries to create an instance of TMXProfilingConnections // with the default settings.
                    .setProfilingConnections(profilingConnections)
                    // (OPTIONAL) Set timeout for entire profiling
                    .setProfileTimeout(20, TimeUnit.SECONDS)
                    // (OPTIONAL) Register for location services
                    // Requires ACCESS_FINE_LOCATION or ACCESS_COARSE_LOCATION permission
                    .setRegisterForLocationServices(true);

            /*
             * Calling init is mandatory to perform initial setup and requires at a minimum,
             * the application context and orgId.
             *
             * Only the first call to init() will use the configuration object, subsequent calls
             * will be ignored. init() can fail due to illegal argument or state in which cases throws * exception to draw attention to the programming problem. Failure cases include malformed * organisation id, malformed fingerprint server
             */
            TMXProfiling.getInstance().init(config);
            //Init was successful or there is a valid instance to be used for further calls. Fire a profile request
            Log.d(TAG, "Successfully init-ed ");
            doProfile();
        }
    }

    protected void requestPermissions() {
        List<String> permissions = new ArrayList<>(2);
        int permissionStatus = ContextCompat.checkSelfPermission(getApplicationContext(),ACCESS_FINE_LOCATION);
        if(permissionStatus != PackageManager.PERMISSION_GRANTED)
        {
            permissions.add(ACCESS_FINE_LOCATION);
        }

        permissionStatus = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        if(permissionStatus != PackageManager.PERMISSION_GRANTED)
        {
            permissions.add(READ_PHONE_STATE);
        }

        // If API version is less than 23 (Android M) adding permissions to AndroidManifest.xml is enough
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions.size() > 0)
        {
            // Request permissions, please note that 1234 is the requestCode which in LemonBank application is not important
            requestPermissions(permissions.toArray(new String[2]), 1234);
        }

    }

    void doProfile() {
        // (OPTIONAL) Assign some custom attributes to be included with the profiling information
        List<String> list = new ArrayList<String>();
        list.add("attribute 1");
        list.add("attribute 2");
        TMXProfilingOptions options = new TMXProfilingOptions().setCustomAttributes(list); // Fire off the profiling request. We could use a more complex request,
        // but the minimum works fine for our purposes.
        TMXProfilingHandle profilingHandle = TMXProfiling.getInstance().profile(options, new CompletionNotifier());
        // Session id can be collected here
                Log.d(TAG, "Session id = "+ profilingHandle.getSessionID());
        /*
         * profilingHandle can also be used to cancel this profile if needed *
         * profilingHandle.cancel();
         * */

    }
    /**
     * Used for notification from the SDK. Any code that needs to be run upon profiling completion
     * should be called from here.
     * <p>
     * Note: Be careful about calling UI update functions from here, as any callbacks will not happen
     * from the UI thread.
     * </p>
     */
    private class CompletionNotifier implements TMXEndNotifier {

        @Override
        public void complete(TMXProfilingHandle.Result result) {
            //Get the session id to use in API call (AKA session query)
            progressDialog.dismiss();
            sessionId = result.getSessionID();
            Log.i("LemonBankCompletion", "Profile completed with: " + result.getStatus().toString()+ " - " + result.getStatus().getDesc());
        }
    }
}

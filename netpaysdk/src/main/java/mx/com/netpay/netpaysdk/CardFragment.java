package mx.com.netpay.netpaysdk;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CpuUsageInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import mx.com.netpay.netpaysdk.model.TokenCardRequest;
import mx.com.netpay.netpaysdk.model.TokenCardResponse;
import mx.com.netpay.netpaysdk.utils.AutoAddTextWatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_BRAND;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_LAST_FOUR;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_TOKEN;

public class CardFragment extends Fragment {

    private EditText etCardNo;
    private EditText etName;
    private EditText etDate;
    private EditText etCvv;

    private TextInputLayout loCardNo;
    private TextInputLayout loDate;
    private TextInputLayout loCvv;

    private String pk;
    private Api api;
    private ProgressDialog progress;

    private boolean isAmex = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.netpay_sdk_fragment_credit_card, container, false);
        final boolean testMode = getActivity().getIntent().getBooleanExtra(Constants.PARAMENTER_IS_TEST, true);
        pk = getActivity().getIntent().getStringExtra(Constants.PARAMENTER_PK);

        etCardNo = (EditText) view.findViewById(R.id.netpay_sdk_etCardNo);
        etName = (EditText) view.findViewById(R.id.netpay_sdk_etName);
        etDate = (EditText) view.findViewById(R.id.netpay_sdk_etDate);
        etCvv = (EditText) view.findViewById(R.id.netpay_sdk_etCvv);
        final ImageView img = (ImageView) view.findViewById(R.id.imgCard);

        etCardNo.addTextChangedListener(new AutoAddTextWatcher(etCardNo, " ", new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(etCardNo.getText().toString().startsWith("3")) {
                    img.setImageDrawable(getContext().getDrawable(R.drawable.netpay_img_amex));
                    isAmex = true;
                } else if(etCardNo.getText().toString().startsWith("4")) {
                    img.setImageDrawable(getContext().getDrawable(R.drawable.netpay_img_visa));
                    isAmex = false;
                }else if(etCardNo.getText().toString().startsWith("5")) {
                    img.setImageDrawable(getContext().getDrawable(R.drawable.netpay_img_mc));
                    isAmex = false;
                } else {
                    img.setImageDrawable(null);
                    isAmex = false;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }));

        etCvv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (etCvv.getRight() - etCvv.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        // create an alert builder
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("CVV");
                        // set the custom layout
                        final View customLayout = getLayoutInflater().inflate(R.layout.netpay_layout_custom_alert, null);
                        ImageView img = (ImageView) customLayout.findViewById(R.id.img_netpay_cvv_alert_image);

                        if(isAmex) {
                            img.setImageDrawable(getContext().getDrawable(R.drawable.netpay_img_cvv_amex));
                            builder.setMessage("Los 4 dígitos al frente de tu tarjeta.");
                        } else {
                            img.setImageDrawable(getContext().getDrawable(R.drawable.netpay_img_cvv_default));                            builder.setMessage("");
                            builder.setMessage("Los 3 dígitos al reverso de tu tarjeta");
                        }

                        builder.setView(customLayout);

                        builder.show();
                        return true;
                    }
                }
                return false;
            }

        });

        loCardNo = (TextInputLayout) view.findViewById(R.id.netpay_sdk_loCardNo);
        loDate = (TextInputLayout) view.findViewById(R.id.netpay_sdk_loDate);
        loCvv = (TextInputLayout) view.findViewById(R.id.netpay_sdk_loCvv);

        Button btnPay = (Button) view.findViewById(R.id.netpay_sdk_btnSend);

        InputFilter[] editFilters = etName.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
        newFilters[editFilters.length] = new InputFilter.AllCaps();
        etName.setFilters(newFilters);

        etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 1 && count == 1) {
                    String mes = etDate.getText().toString().trim();
                    etDate.setText(mes + "/");
                    etDate.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                api = initRetrofit(testMode);
                loCardNo.setError(null);
                loDate.setError(null);
                loCvv.setError(null);
                if(validateFields()) {
                    Log.d("validateFields","validateFields");
                    sendRequest(v);
                }
            }
        });


        return view;
    }

    public boolean isValid(String cardNumber){
        if(cardNumber.isEmpty()) {
            return false;
        }
        int sum = 0 ;
        boolean even = false;
        String  cardType = null;

        int firstDigit = getDigit(cardNumber,1);
        switch (firstDigit){
            case 4:
                cardType = "Visa";
                break;
            case 3:
                if (getDigit(cardNumber,2)==7) {
                    cardType= "American Express";
                } else {
                    return false;
                }
            case 5:
                cardType = "Master Card";
                break;
            case 6:
                cardType = "Discover";
                break;
        } //end card type switch

        int secondDigit = getDigit(cardNumber, 2);

        //loops through digits in reverse order right to left
        for (int i = cardNumber.length();i>0; i--){
            int digit = getDigit(cardNumber, i);

            //double every other digit
            if (even)
                digit += digit;

            even = ! even;

            //if result is greater than 9 then subtract 9
            if (digit > 9)
                digit = digit - 9;

            sum += digit;
        } //end of loop

        if (sum % 10 == 0) {
            System.out.println("is valid");
            System.out.println("Card Type: "+cardType);
            return true;
        }
        else {
            System.out.println(" is invalid");
            System.out.println("Card Type: Invalid");
            return false;
        }
    } //end of isValid class

    //gets digit at specified position
    private static int getDigit(String digitString, int position){
        String characterAtPosition = digitString.substring(position - 1, position);
        return Integer.parseInt(characterAtPosition);
    }


    private boolean validateFields() {

        boolean isValid = true;

        String cardNo = etCardNo.getText().toString().replace(" ", "");
        if(cardNo.length() < 15 && !isValid(cardNo)) {
            isValid = false;
            loCardNo.setError(getActivity().getResources().getString(R.string.str_invalidcard));
        }

        if(etDate.getText().toString().length() < 5) {
            isValid = false;
            loDate.setError(getActivity().getResources().getString(R.string.str_invaliddate));
        } else {
            String date = etDate.getText().toString();
            String month = date.split("/")[0];
            String year = date.split("/")[1];

            if(Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
                loDate.setError(getActivity().getResources().getString(R.string.str_invaliddate));
                isValid = false;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH,1);
            calendar.set(Calendar.MONTH,Integer.parseInt(month));
            calendar.set(Calendar.YEAR,Integer.parseInt("20"+year));

            Date expDate = calendar.getTime();

            if(new Date().compareTo(expDate) >= 0) {
                loDate.setError(getActivity().getResources().getString(R.string.str_exp_card));
                isValid = false;
            }

        }

        if(etCvv.getText().toString().length() < 3) {
            loCvv.setError(getActivity().getResources().getString(R.string.str_invalidcvv));
            isValid = false;
        }


        return isValid;
    }

    private void sendRequest(final View v) {
        progress = ProgressDialog.show(getContext(), "Por favor espere...",
                "Generando token", true);

        TokenCardRequest tokenCardRequest = new TokenCardRequest();
        tokenCardRequest.setCardNumber(etCardNo.getText().toString().replace(" ",""));
        tokenCardRequest.setCardHolderName(etName.getText().toString());
        tokenCardRequest.setCvv2(etCvv.getText().toString());
        tokenCardRequest.setExpMonth(etDate.getText().toString().split("/")[0]);
        tokenCardRequest.setExpYear(etDate.getText().toString().split("/")[1]);

        String token = ((BaseActivity) requireContext()).sessionId;

        tokenCardRequest.setDeviceFingerPrint(token);

        api.requestCardToken(pk,tokenCardRequest).enqueue(new Callback<TokenCardResponse>() {
            @Override
            public void onResponse(Call<TokenCardResponse> call, Response<TokenCardResponse> response) {

                if(response.isSuccessful()) {
                    TokenCardResponse tokenCardResponse = response.body();

                    Bundle b = new Bundle();
                    b.putString(RESPONSE_TOKEN,tokenCardResponse.getToken());
                    b.putString(RESPONSE_LAST_FOUR,tokenCardResponse.getLastFourDigits());
                    b.putString(RESPONSE_BRAND,tokenCardResponse.getBrand());

                    Intent i = new Intent();
                    i.putExtras(b);

                    getActivity().setResult(RESULT_OK,i);
                    getActivity().finish();
                } else {
                    TokenCardResponse tokenCardResponse = null;
                    String errorMessage;
                    try {
                        tokenCardResponse = new Gson().fromJson(response.errorBody().string(), TokenCardResponse.class);
                        errorMessage = tokenCardResponse.getMessage();
                    } catch (IOException e) {
                        errorMessage = "Ocurrió un error al obtener el token";
                    }

                    Snackbar.make(v,errorMessage,Snackbar.LENGTH_LONG).show();
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<TokenCardResponse> call, Throwable t) {
                Log.d("onFailure",t.getMessage());
                Snackbar.make(v,t.getMessage(),Snackbar.LENGTH_LONG).show();
                progress.dismiss();
            }
        });


    }

    private static Api initRetrofit(boolean testMode) {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(testMode?"https://gateway-154.netpaydev.com/gateway-ecommerce/":"https://suite.netpay.com.mx/gateway-ecommerce/");

        retrofitBuilder.client(client);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        return retrofit.create(Api.class);
    }

    // ------------------------- PATTERN DEFINE TYPE CREDIT --------------------------- //
    private static final String VISA = "^4.*";
    private static final String MASTER_CARD = "^5[1-5].*";
    private static final String AMEX = "^3[47].*";

    // ------------------------- PATTERN SEPARATOR GROUP NUMBERS --------------------------- //
    private static final String VISA_SEPARATOR =
            "\\b([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})\\b";
    private static final String AMEX_SEPARATOR =
            "\\b([0-9]{4})([0-9]{6})([0-9]{5})\\b";
}

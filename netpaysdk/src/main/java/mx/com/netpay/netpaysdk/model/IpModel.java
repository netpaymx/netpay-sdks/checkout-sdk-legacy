package mx.com.netpay.netpaysdk.model;

public class IpModel {
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

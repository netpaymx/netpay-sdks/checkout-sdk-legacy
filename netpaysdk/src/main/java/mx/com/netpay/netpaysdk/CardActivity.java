package mx.com.netpay.netpaysdk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import static mx.com.netpay.netpaysdk.Constants.RESPONSE_ERROR;

public final class CardActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.netpay_sdk_activity_credit_card);

        setTitle("");
        try {
            Toolbar t = findViewById(R.id.netpay_sdk_toolbar);
            setSupportActionBar(t);

            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(0xFFFFFFFF);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back);
            upArrow.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

        }catch (Exception e) {

        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putString(RESPONSE_ERROR,"Cancelado por el usuario");
        Intent i = new Intent();
        i.putExtras(b);
        setResult(Activity.RESULT_CANCELED, i);
        super.onBackPressed();
    }

}
